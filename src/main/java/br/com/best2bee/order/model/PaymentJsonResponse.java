package br.com.best2bee.order.model;

import java.util.ArrayList;

public class PaymentJsonResponse {
	Headers HeadersObject;
	Body BodyObject;
	private float statusCodeValue;
	private String statusCode;

	// Getter Methods

	public Headers getHeaders() {
		return HeadersObject;
	}

	public Body getBody() {
		return BodyObject;
	}

	public float getStatusCodeValue() {
		return statusCodeValue;
	}

	public String getStatusCode() {
		return statusCode;
	}

	// Setter Methods

	public void setHeaders(Headers headersObject) {
		this.HeadersObject = headersObject;
	}

	public void setBody(Body bodyObject) {
		this.BodyObject = bodyObject;
	}

	public void setStatusCodeValue(float statusCodeValue) {
		this.statusCodeValue = statusCodeValue;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
}

final class Body {
	private float orderId;
	private String orderNumber = null;
	private String status;
	ArrayList<Object> orderItems = new ArrayList<Object>();
	Customer CustomerObject;
	PaymentDetails PaymentDetailsObject;

	// Getter Methods

	public float getOrderId() {
		return orderId;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public String getStatus() {
		return status;
	}

	public Customer getCustomer() {
		return CustomerObject;
	}

	public PaymentDetails getPaymentDetails() {
		return PaymentDetailsObject;
	}

	// Setter Methods

	public void setOrderId(float orderId) {
		this.orderId = orderId;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setCustomer(Customer customerObject) {
		this.CustomerObject = customerObject;
	}

	public void setPaymentDetails(PaymentDetails paymentDetailsObject) {
		this.PaymentDetailsObject = paymentDetailsObject;
	}
}

final class Headers {

	// Getter Methods

	// Setter Methods

}