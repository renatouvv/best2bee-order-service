package br.com.best2bee.order.utils;

import com.google.gson.JsonObject;

import br.com.best2bee.order.model.PaymentDetails;

public interface Connection {

	public JsonObject getConnection(String url, PaymentDetails details);
	
	public JsonObject deserialize(String json);
	
}