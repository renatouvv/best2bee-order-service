package br.com.best2bee.order.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import br.com.best2bee.order.model.PaymentDetails;

@Component
public class ConnectionImpl implements Connection {

	@Override
	public JsonObject getConnection(String url, PaymentDetails details) {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost httpPost = new HttpPost("http://localhost:8088/api/payment" + url);
		httpPost.addHeader("content-type", "application/json");
		
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"cardNumber\" : ");
		json.append("\"" + details.getCardNumber() + "\",");
		json.append("\"cvv\" : ");
		json.append("\"" + details.getCvv() + "\",");
		json.append("\"expirationDate\" : ");
		json.append("\"" + details.getExpirationDate() + "\",");
		json.append("\"flag\" : ");
		json.append("\"" + details.getFlag() + "\"");
		json.append("}");
		
//		List<NameValuePair> urlParameters = new ArrayList<>();
//		urlParameters.add(new BasicNameValuePair("cardNumber", details.getCardNumber()));
//		urlParameters.add(new BasicNameValuePair("cvv", details.getCvv()));
//		urlParameters.add(new BasicNameValuePair("expirationDate", details.getExpirationDate()));
//		urlParameters.add(new BasicNameValuePair("flag", details.getFlag()));
		
		JsonObject jsonObject = new JsonObject();
		
		try {
//			httpPost.setEntity(new UrlEncodedFormEntity(urlParameters));
			httpPost.setEntity(new StringEntity(json.toString()));
			
			HttpResponse response = httpClient.execute(httpPost);

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader((response.getEntity().getContent())));

			String line;
			StringBuilder stringBuilder = new StringBuilder();
			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line);
			}
			
			jsonObject = deserialize(stringBuilder.toString());
	        bufferedReader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return jsonObject;
	}

	@Override
	public JsonObject deserialize(String json) {
		Gson gson = new Gson();
		JsonObject jsonClass = gson.fromJson(json, JsonObject.class);
		return jsonClass;
	}

}