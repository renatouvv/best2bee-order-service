package br.com.best2bee.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.best2bee.order.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}