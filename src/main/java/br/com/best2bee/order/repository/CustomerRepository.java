package br.com.best2bee.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.best2bee.order.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}