package br.com.best2bee.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.best2bee.order.model.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

}