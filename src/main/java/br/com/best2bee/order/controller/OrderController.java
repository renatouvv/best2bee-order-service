package br.com.best2bee.order.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.best2bee.order.model.Customer;
import br.com.best2bee.order.model.Order;
import br.com.best2bee.order.model.Product;
import br.com.best2bee.order.service.OrderService;

@Controller
@Path("/order")
public class OrderController {

	@Autowired
	OrderService service;

	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

	@Path("/new")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Order> newOrder(String jsonRequest) {
		System.out.println("Request: " + jsonRequest);

		Order order = new Order();
		ResponseEntity<Order> response;
		ObjectMapper mapper = new ObjectMapper();
		boolean isPaymentOk = false;

		try {
			order = mapper.readValue(jsonRequest, Order.class);
			order = service.createNewOrder(order);
			isPaymentOk = service.processPayment(order.getPaymentDetails());
		} catch (Exception e) {
			logger.error("Ocorreu um erro ao tentar inserir uma nova ordem." + "\n Mensagem: " + e.getMessage()
					+ "\n Causa: " + e.getCause());
		}
		
		if (isPaymentOk) {
			order.setStatus("Pago");
			response = new ResponseEntity<Order>(order, HttpStatus.OK);
		} else {
			response = new ResponseEntity<Order>(new Order(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return response;
	}
	
	@Path("/customer/new")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Customer> newCustomer(String jsonRequest) {
		Customer customer;
		ResponseEntity<Customer> response;
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			customer = mapper.readValue(jsonRequest, Customer.class);
			customer = service.createNewCustomer(customer);
			response = new ResponseEntity<Customer>(customer, HttpStatus.OK);
		} catch (Exception e) {
			response = new ResponseEntity<Customer>(new Customer(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Ocorreu um erro ao tentar inserir um novo produto." + "\n Mensagem: " + e.getMessage()
					+ "\n Causa: " + e.getCause());
		}
		
		return response;
	}
	
	@Path("/product/new")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ResponseEntity<Product> newProduct(String jsonRequest) {
		Product product;
		ResponseEntity<Product> response;
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			product = mapper.readValue(jsonRequest, Product.class);
			product = service.createNewProduct(product);
			response = new ResponseEntity<Product>(product, HttpStatus.OK);
		} catch (Exception e) {
			response = new ResponseEntity<Product>(new Product(), HttpStatus.INTERNAL_SERVER_ERROR);
			logger.error("Ocorreu um erro ao tentar inserir um novo produto." + "\n Mensagem: " + e.getMessage()
					+ "\n Causa: " + e.getCause());
		}
		
		return response;
	}

}