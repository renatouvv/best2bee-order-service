package br.com.best2bee.order.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import br.com.best2bee.order.model.Customer;
import br.com.best2bee.order.model.Order;
import br.com.best2bee.order.model.PaymentDetails;
import br.com.best2bee.order.model.PaymentJsonResponse;
import br.com.best2bee.order.model.Product;
import br.com.best2bee.order.repository.CustomerRepository;
import br.com.best2bee.order.repository.OrderRepository;
import br.com.best2bee.order.repository.ProductRepository;
import br.com.best2bee.order.utils.Connection;

@Service
public class OrderServiceImp implements OrderService {
	
	@Autowired
	OrderRepository orderRepo;
	
	@Autowired
	CustomerRepository customerRepo;
	
	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	Connection conn;

	@Override
	public Order createNewOrder(Order order) {
		return orderRepo.save(order);
	}

	@Override
	public Customer createNewCustomer(Customer customer) {
		return customerRepo.save(customer);
	}

	@Override
	public Product createNewProduct(Product product) {
		return productRepo.save(product);
	}

	@Override
	public boolean processPayment(PaymentDetails paymentDetails) {
			
		boolean result = false;

		String url = "/process";

		JsonObject jsonObject = conn.getConnection(url, paymentDetails);
		
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();
		PaymentJsonResponse response = gson.fromJson(jsonObject, PaymentJsonResponse.class);
		
		if (response.getStatusCodeValue() == 200) {
			result = true;
		}

		return result;
	}

}