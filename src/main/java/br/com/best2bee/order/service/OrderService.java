package br.com.best2bee.order.service;

import br.com.best2bee.order.model.Customer;
import br.com.best2bee.order.model.Order;
import br.com.best2bee.order.model.PaymentDetails;
import br.com.best2bee.order.model.Product;

public interface OrderService {
	
	public Customer createNewCustomer(Customer customer);
	
	public Order createNewOrder(Order order);
	
	public Product createNewProduct(Product product);
	
	public boolean processPayment(PaymentDetails paymentDetails);

}