package br.com.best2bee.order;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.best2bee.order.controller.OrderController;

@SpringBootApplication
public class OrderServiceApplication {
	
	@Bean
	ResourceConfig resourceConfig() {
		return new ResourceConfig().register(OrderController.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(OrderServiceApplication.class, args);
	}

}
